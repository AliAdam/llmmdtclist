//
//  SplashViewController.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
/// spalsh screen if the need to download configration
/// or some thing before starting the app
class SplashViewController: AppBaseViewController {
    fileprivate let router: SplashRouter

    init(router: SplashRouter) {
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // add background image
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // go to  Deliveries List screen
        router.navigateDeliveriesListscreen()
    }
}


// MARK: Setup
private extension SplashViewController {

    func setupViews() {
        self.addbackgroundImage()
        
    }
}
