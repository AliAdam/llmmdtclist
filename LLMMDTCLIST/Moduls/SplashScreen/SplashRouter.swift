//
//  SplashRouter.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import Foundation
import UIKit
///push other Scenes
class SplashRouter {
    weak var viewController: SplashViewController?
    
    // go to  Deliveries List screen
    func navigateDeliveriesListscreen() {
        let rootController = ListBuilder.viewController()
        let navigationController = UINavigationController(rootViewController: rootController)
        UIApplication.shared.keyWindow?.rootViewController = navigationController;
    }
}
