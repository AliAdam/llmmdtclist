//
//  AppBaseViewController.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 1/20/18.
//  Copyright © 2018 Ali Adam. All rights reserved.
//

import UIKit
import Reachability
import RxSwift
import RxCocoa
import ESPullToRefresh

 
class AppBaseViewController: UIViewController {
    var disposeBag = DisposeBag()

    open var backBtnImageName:String = "back"
    
    open var _hasBackButton:Bool = false;

    override var title: String? {
        didSet {
            if let navTitle = navigationItem.title {
                setTNavBarTitleAsLabel(title: navTitle)
            }
        }
    }
    
    func setTNavBarTitleAsLabel(title: String){
        let navigationTitlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        navigationTitlelabel.numberOfLines = 1
        navigationTitlelabel.lineBreakMode = .byTruncatingTail
        navigationTitlelabel.font = FontManager.appNavTitleFont
        navigationTitlelabel.adjustsFontSizeToFitWidth = true
        navigationTitlelabel.minimumScaleFactor = 0.1
        navigationTitlelabel.textAlignment = .center
        navigationTitlelabel.textColor  = ColorsManager.whiteColor
        navigationTitlelabel.text = title
        self.navigationItem.titleView = navigationTitlelabel
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupdbErrorSubject()
        ReachabilityManager.shared.startMonitoring()
        ReachabilityManager.shared.addListener(listener: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()

    }
    
    func setupdbErrorSubject() {
    RealmService.shared.dbErrorSubject
    .asObservable()
    .subscribe({  error in
    ToastUtils.hide()
    ToastUtils.showErrorToast(msg:error.debugDescription )
    })
    .disposed(by: disposeBag)
    

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    private func setupNavigationBar() {
        setupBackBtn()

    }
    private func setupBackBtn() {
        if (_hasBackButton) {
            if (self.navigationItem.leftBarButtonItem == nil && ((self.navigationController != nil && (self.navigationController!.viewControllers as NSArray).count > 1))) {
                
                self.navigationItem.hidesBackButton = true;
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: self.backBtnImageName), style:UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonTapped));
                
            }
        } else {
            self.navigationItem.hidesBackButton = true;
        }
    } 
    
    ///Navigation back button tap handler.
    @IBAction func backBTNAction(_ sender: Any) {
        
       self.backButtonTapped()
    }
    @objc open func backButtonTapped() {
        self.view.endEditing(true);
        
        if (self.navigationController?.viewControllers.count == 1) {
            self.dismiss(animated: true, completion: nil)
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    } 
 
    

    func addbackgroundImage()  {
        let img = UIImage(named:"background")
        let imageView = UIImageView(image: img)
        self.view.addSubview(imageView)
        imageView.bindFrameToSuperviewBounds()
    }
    
}

extension AppBaseViewController: NetworkStatusListener {

    func networkStatusDidChange(status: Reachability.Connection) {

        switch status {
        case .none:
            ToastUtils.showErrorToast(msg: LocalizableWords.connectionError)
        default :
            break

        }
    }
}


