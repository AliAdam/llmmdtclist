//
//  SplashViewModel.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 1/26/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import  Foundation
import RxSwift
import RxCocoa

class ViewModel :NSObject{
   let disposeBag = DisposeBag()
    let successSubject = PublishSubject<String>()
    let errorSubject = PublishSubject<String>()
    let canLoadMoreSubject = PublishSubject<Bool>()
    var offset = 0
    var limit = 20

    override init() {
        super.init()

    }
    
}



