//
//  ListViewModel.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import RxSwift

class ListViewModel:ViewModel {

    // deliveries List
    var deliveriesList: Variable<[Delivery]> = Variable([])


    override init() {
        super.init()
    }
    // init for unit testing
    init(deliveriesList: [Delivery]) {
        super.init()
        self.deliveriesList.value = deliveriesList
        RealmService.shared.deleteAll()
        RealmService.shared.create(deliveriesList)
    }
    // update selected item on date base
    func updateSelectedItem(with index: Int) {
        try! RealmService.shared.realm.write {
            deliveriesList.value.filter({$0.isSelected}).forEach({$0.isSelected = false})
            let pred = NSPredicate(format: "isSelected = true")
            RealmService.shared.getObjectsWith(type: Delivery.self , filter: pred).forEach({$0.isSelected = false})
            deliveriesList.value[index].isSelected = true
        }
    }
    // load offline list
    func loadOfflineDeliveriesList()  {
    deliveriesList.value = RealmService.shared.getObjectsWith(type: Delivery.self)
    }
    // load data from server
    func loadDeliveriesList(){
        NetworkProvider.shared.getDeliveresList(offset: self.offset, limit: self.limit){ (response) in
            switch response {
            case let .success(list):
                if self.offset == 0 {
                    self.successSubject.onNext(LocalizableWords.newDataComes)
                    RealmService.shared.deleteAll()
                    self.deliveriesList.value = list
                }
                else {
                    self.deliveriesList.value.append(contentsOf:list)
                }
                RealmService.shared.create(list)
                self.canLoadMoreSubject.onNext(list.count < self.limit)
            case let .error(error):
                   self.errorSubject.onNext("\(error.localizedDescription)")
                self.loadDeliveriesList()
                debugPrint("\(error.localizedDescription)")
            }
        }
    }
     // refresh list
    func refresh() {
        self.offset = 0
        loadDeliveriesList()
    }
    
    // load next page 
    func loadMore() {
        self.offset = self.offset + self.limit
        loadDeliveriesList()
        
    }
}

