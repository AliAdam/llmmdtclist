//  ListViewController.swift
//  LLMMDTCLIST
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa

class ListViewController: AppBaseViewController {
    fileprivate let viewModel: ListViewModel
    fileprivate let router: ListRouter
    //MARK: - Variables
    
    // create table view
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.rowHeight = 120
        tableView.register(DeliveryCell.self, forCellReuseIdentifier: DeliveryCell.identifier())
        tableView.separatorStyle = .none
        return tableView
    }()
    
    //MARK: - Methods
    init(withViewModel viewModel: ListViewModel, router: ListRouter) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // add tableview and setup title  and background image
        setupViews()
        // add Constraints
        makeConstraints()
        //binding viewmodel to tableview and observe other variables
        setupRx()
        // load offline data to show it until new data come
        loadData()
    }
}
// MARK: Setup
private extension ListViewController {
    // add tableview and setup title  and background image
    func setupViews() {
        self.title = LocalizableWords.deliveriesList
        addbackgroundImage()
        self.view.addSubview(tableView)
    }
    
    // add Constraints
    func makeConstraints() {
        tableView.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.view).inset(UIEdgeInsets(top: 100, left: 0, bottom: 0, right: 0))
        })
    }
    
    
    //binding viewmodel to tableview and observe other variables
    func setupRx() {
        bindViewModelToTableView()
    
        listenToViewModel()
    }
    
    func bindViewModelToTableView()  {
        // listen to changes on deliveries List on viewmodel and update the table based on it
        self.viewModel.deliveriesList.asObservable()
            .bind(to: self.tableView.rx.items(cellIdentifier: DeliveryCell.identifier(), cellType: DeliveryCell.self)) { (row, element, cell) in
                cell.delivery = element
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()
            }
            .disposed(by: disposeBag)
        
        // add pull to refresh to the table and call viewmodel refrsh
        self.tableView.es.addPullToRefresh { [weak self] in
            self?.viewModel.refresh()
        }
        // add load more to the table
        self.addloadMoreToTable()
        
        // subscribe to tableview cell selected and
        //update the date base then go to details screen
        tableView.rx.itemSelected
            .subscribe(onNext: {[weak self]  indexPath in
                self?.viewModel.updateSelectedItem(with: indexPath.row)
                self?.router.navigateToDetails()
            }).disposed(by:disposeBag)
        
        // subscribe to can Load More Subject from viewmodel to stop loading more
        // when data finished
        viewModel.canLoadMoreSubject.asObservable().subscribe({ [weak self] isEmpty in
            if (isEmpty.element ?? false) {
                ToastUtils.hide()
                self?.tableView.es.noticeNoMoreData()
                self?.tableView.es.removeRefreshFooter()
            }
        }).disposed(by: disposeBag)
        
    }
    
    func listenToViewModel()  {
        // subscribe to success Subject from viewModel to show Success Toast
        viewModel.successSubject
            .asObservable()
            .subscribe({  message in
                ToastUtils.showSuccessToast(msg:message.element ?? "")
                self.addloadMoreToTable()
            })
            .disposed(by: disposeBag)
        
        // subscribe to error Subject from viewModel
        viewModel.errorSubject
            .asObservable()
            .subscribe({  message in
                self.tableView.es.stopLoadingMore()
                self.tableView.es.stopPullToRefresh()
            })
            .disposed(by: disposeBag)
    }
    // load offline data to show it until new data come
    func loadData() {
        self.viewModel.loadOfflineDeliveriesList()
        if self.viewModel.deliveriesList.value.count > 0 {
            ToastUtils.showInfoToast(msg: LocalizableWords.loading)
        }
        else {
            ToastUtils.showLoadingToast(msg: LocalizableWords.loadingNew)
        }
        self.viewModel.loadDeliveriesList()
    }
    // add load more to tableView
    func addloadMoreToTable()   {
        self.tableView.es.resetNoMoreData()
        self.tableView.es.addInfiniteScrolling { [weak self] in
            self?.viewModel.loadMore()
        }
    }
}
