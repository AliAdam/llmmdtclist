//
//  ListRouter.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import Foundation

///push other Scenes
class ListRouter {
    weak var viewController: ListViewController?

    // navigate To Details screen
    func navigateToDetails() {
        let rootController = DetailsBuilder.viewController()
        viewController?.navigationController?.pushViewController(rootController, animated: true)
    }
}
