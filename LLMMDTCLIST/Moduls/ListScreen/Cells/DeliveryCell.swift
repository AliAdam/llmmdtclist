//
//  DeliveryCell.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright © 2018 Ali Adam. All rights reserved.
//

import UIKit
import SnapKit


class DeliveryCell: UITableViewCell {
    
    //MARK: - Variables
    
    var delivery: Delivery? {
        didSet {
            setItemInfo()
        }
    }
    
    //  create Delivery View
    let itemView : DeliveryItemView = {
        let itemView = DeliveryItemView()
        return itemView
    }()
    
    //MARK: - Methods
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // add Delivery  View and constraints  to the cell
        addSubviews()
        makeConstraints()
        
    }
    
    fileprivate func addSubviews() {
        self.addSubview(itemView)
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.selectionStyle = .none
    }
    
    fileprivate func makeConstraints() {
        itemView.snp.makeConstraints({ (make) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10))
        })
    }
    
    // return cell identifier
    open class func identifier() -> String {
        return String(describing: DeliveryCell.self)
    }
    
    // show info on the view
    private func setItemInfo() {
        self.itemView.itemLabel.text = self.delivery?.descriptionField ?? ""
        self.itemView.itemAddressLabel.text = self.delivery?.location?.address ?? ""
        if let url = self.delivery?.imageUrl{
            self.itemView.itemImageView.loadImageFromUrl(url)
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

