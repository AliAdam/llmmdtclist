//
//  DetailsBuilder.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import UIKit

///initialize ViewController, ViewModel and Router
/// provide the ViewController to the outside world (via the viewController() method)
//  if the Scene needs some initial data it is injected into the Builder who then injects it into the ViewModel

struct DetailsBuilder {

    static func viewController() -> UIViewController {
        let viewModel = DetailsViewModel()
        let viewController = DetailsViewController(withViewModel: viewModel)
        return viewController
    }
}
