//
//  DetailsViewModel.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import RxSwift
import MapKit
class DetailsViewModel:ViewModel {

    // selected delivery Item
    let deliveryItem: Variable<Delivery> = Variable(Delivery())
    
    override init() {
        super.init()
    }
    // get selected item to show its details
    func loadSelectedItem()  {
        let pred = NSPredicate(format: "isSelected = true")
        deliveryItem.value =  RealmService.shared.getObjectsWith(type: Delivery.self , filter: pred).first!
    }
     // convert item lat ,long to CLLocationCoordinate2D
    func getloction() -> CLLocationCoordinate2D {
        let coord = CLLocationCoordinate2D(latitude: Double((deliveryItem.value.location?.lat)!), longitude: Double((deliveryItem.value.location?.lng)!))
        return coord
    }
    
    
}
