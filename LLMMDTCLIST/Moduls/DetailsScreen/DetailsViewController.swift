//
//  DetailsViewController.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright (c) 2018 Ali Adam. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MapKit
import SnapKit

class DetailsViewController: AppBaseViewController {
    fileprivate let viewModel: DetailsViewModel
    // create mapView
    var mapView :MKMapView = {
        let mapView = MKMapView()
        mapView.showsUserLocation = false
        return mapView
    }()
     // create Delivery Item View
    let itemView : DeliveryItemView = {
        let itemView = DeliveryItemView()
        itemView.layer.borderColor = ColorsManager.brandColor.cgColor
        itemView.layer.borderWidth = 1.5
        return itemView
    }()
    
    init(withViewModel viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        makeConstraints()
        setupRx()
    }
}

// MARK: Setup
private extension DetailsViewController {
    // add map and Delivery item view
    func setupViews() {
        self.title = LocalizableWords.deliverydet
        self._hasBackButton = true
        self.addbackgroundImage()
        self.view.addSubview(self.mapView)
        self.view.addSubview(self.itemView)
    }
    // add Constraints to views
    func makeConstraints() {
        mapView.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.view)
        })
        itemView.snp.makeConstraints { (make) in
            let offset = 20
            let height = 100
            make.leftMargin.equalTo(self.view.snp.leftMargin).offset(offset)
            make.bottomMargin.equalTo(self.view.snp.bottomMargin).offset(-offset)
            make.rightMargin.equalTo(self.view.snp.rightMargin).offset(-offset)
            make.height.equalTo(height)
        }
    }

    // bind viewmodel data
    func setupRx() {
        self.viewModel.deliveryItem.asObservable().subscribe(onNext:{ item in
            if let desc = item.descriptionField {
                self.itemView.itemLabel.text = desc
                self.itemView.itemAddressLabel.text = item.location?.address ?? ""
                if let url = item.imageUrl{
                    self.itemView.itemImageView.loadImageFromUrl(url)
                }
                self.showLocationOnMap(coordinate: self.viewModel.getloction(), title:desc)
            }
        }).disposed(by: disposeBag)
        self.viewModel.loadSelectedItem()
    }
    // show driver location on the map
    func showLocationOnMap(coordinate :CLLocationCoordinate2D,title:String) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = title
        self.mapView.addAnnotation(annotation)
        let regionRadius: CLLocationDistance = 1000
        let meters = regionRadius * 2.0
        let coordinateRegion = MKCoordinateRegion(center: coordinate,latitudinalMeters: meters, longitudinalMeters:meters)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
