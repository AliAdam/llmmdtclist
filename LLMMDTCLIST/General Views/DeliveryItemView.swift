//
//  DeliveryItemView.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright © 2018 Ali Adam. All rights reserved.
//

import UIKit
import SnapKit

/// custom view used on Delivery Cell and details controller
/// contain imageview and description label and address label
class DeliveryItemView: UIView {

//MARK: - Variables
    /// create image view
    var itemImageView: UIImageView = {
        let itemImageView = UIImageView()
        itemImageView.contentMode = .scaleAspectFill
        itemImageView.clipsToBounds = true
        return itemImageView
    }()
    
    ///descreption label
    var itemLabel: UILabel = {
        let itemLabel = UILabel()
        itemLabel.numberOfLines = 0
        itemLabel.backgroundColor = UIColor.clear
        itemLabel.font = FontManager.appHeaderFont
        itemLabel.textColor = ColorsManager.blackColor
        return itemLabel
    }()
    /// address label
    var itemAddressLabel: UILabel = {
        let itemAddressLabel = UILabel()
        itemAddressLabel.numberOfLines = 0
        itemAddressLabel.backgroundColor = UIColor.clear
        itemAddressLabel.font = FontManager.appDetailsFont
        itemAddressLabel.textColor = ColorsManager.brandGray
        return itemAddressLabel
    }()
    
    /// address icon
    var addressIconImageView: UIImageView = {
        let addressIconImageView = UIImageView()
        addressIconImageView.contentMode = .scaleAspectFill
        addressIconImageView.clipsToBounds = true
        addressIconImageView.image = #imageLiteral(resourceName: "location-icon")
        return addressIconImageView
    }()
    
    //MARK:- Methods
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        makeConstraints()
        setupViews()
    }
    
    fileprivate func setupViews() {
        self.layer.cornerRadius = 15
        self.backgroundColor = ColorsManager.whiteColor
        self.clipsToBounds = true
    }
    /// add subview to the view
    fileprivate func addSubviews() {
        self.addSubview(self.itemImageView)
        self.addSubview(self.itemLabel)
        self.addSubview(self.itemAddressLabel)
        self.addSubview(self.addressIconImageView)
    }
     /// add  Constraints to each View
    fileprivate func makeConstraints() {
        itemImageView.snp.makeConstraints({ (make) in
            make.leftMargin.equalTo(snp.leftMargin)
            make.bottomMargin.equalTo(snp.bottomMargin)
            make.topMargin.equalTo(snp.topMargin)
            make.width.equalTo(100)
            make.height.equalToSuperview()
        })
        
        itemLabel.snp.makeConstraints({ (make) in
            make.leftMargin.equalTo(self.itemImageView.snp.rightMargin).offset(30)
            make.rightMargin.equalTo(snp.right).offset(-30)
            make.topMargin.equalTo(snp.topMargin).offset(10)
        })
        
        itemAddressLabel.snp.makeConstraints({ (make) in
            make.leftMargin.equalTo(self.addressIconImageView.snp.rightMargin).offset(20)
            make.rightMargin.equalTo(snp.right)
            make.topMargin.equalTo(self.itemLabel.snp.bottomMargin).offset(20)
            make.bottomMargin.equalTo(snp.bottomMargin).offset(-10)
        })
        
        addressIconImageView.snp.makeConstraints({ (make) in
            make.leftMargin.equalTo(self.itemImageView.snp.rightMargin).offset(30)
            make.topMargin.equalTo(self.itemLabel.snp.bottomMargin).offset(20)
            make.width.height.equalTo(30)
            make.bottomMargin.equalTo(snp.bottomMargin).offset(-10)
        })
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
