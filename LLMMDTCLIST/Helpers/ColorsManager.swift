

import UIKit
// colors used inside the app 
struct ColorsManager {

    static let  brandColor   = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
    static let  brandGray   = #colorLiteral(red: 0.6549019608, green: 0.662745098, blue: 0.6745098039, alpha: 1)
    static let  blackColor = #colorLiteral(red: 0.2666336298, green: 0.2666856647, blue: 0.2666303515, alpha: 1)
    static let  whiteColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)



}



