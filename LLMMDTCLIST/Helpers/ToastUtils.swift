//
//  ToastUtils.swift
//  LLMMDTCLIST
//
//  Created by ali.adam on 9/28/18.
//  Copyright © 2018 ali.adam.LLMMDTCLIST. All rights reserved.
//

import UIKit
import SVProgressHUD

class ToastUtils: NSObject {
    class func showErrorToast(msg:String) {
        SVProgressHUD.dismiss()
        SVProgressHUD.showError(withStatus: msg)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            SVProgressHUD.dismiss()
        }
    }



    class func showSuccessToast(msg:String) {
        SVProgressHUD.dismiss()
        SVProgressHUD.showSuccess(withStatus: msg)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            SVProgressHUD.dismiss()
        }
    }



    class func showInfoToast(msg:String) {
        SVProgressHUD.dismiss()
        SVProgressHUD.showInfo(withStatus: msg)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            SVProgressHUD.dismiss()
        }
    }


    class func showLoadingToast(msg:String) {
        SVProgressHUD.dismiss()
        SVProgressHUD.show(withStatus: msg)
    }

    class func hide(){
        SVProgressHUD.dismiss()
    }

}
