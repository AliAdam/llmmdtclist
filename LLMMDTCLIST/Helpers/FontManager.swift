//
//  FontManager.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 3/8/17.
//  Copyright © 2017 Ali Adam. All rights reserved.
//

import UIKit

// fonts used inside the app
enum FontManager:String {
    case APPRegular
    case APPMedium
    case APPSemiBold
    case APPLight
    
    static let appHeaderFont = FontManager.APPSemiBold.fontWith(size: 16)
    static let appNavTitleFont = FontManager.APPRegular.fontWith(size: 20)
    static let appDetailsFont = FontManager.APPLight.fontWith(size: 15)
    
   private var fontName:String {
        return self.englishFontName

        //return true ? self.arabicFontName :self.englishFontName
    }
  
    private var englishFontName:String {
        switch self {
            case .APPRegular: return "AvenirNext-Regular"
            case .APPMedium: return "Avenir-Medium"
            case .APPLight: return "Avenir-Light"
            case .APPSemiBold: return "AvenirNext-Regular"
        }
    }
    
    private var arabicFontName:String {
        switch self {
        case .APPRegular: return "AvenirNext-Regular"
        case .APPMedium: return "Avenir-Medium"
        case .APPLight: return "Avenir-Light"
        case .APPSemiBold: return "AvenirNext-Regular"
        }
    }
    
   public func fontWith(size: CGFloat) -> UIFont {
    if let font = UIFont(name:self.fontName,size:size) {
        return font
    }
    return UIFont.systemFont(ofSize:size)
    }
}


