//
//  LocalizableWords.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 1/26/18.
//  Copyright © 2018 Ali Adam. All rights reserved.
//

import UIKit
// here you can find  constant messages and any other constant words on the App
struct LocalizableWords  {
    
    static let Error = "Error"
    static let Ok = "ok"
    static let loading = "go offline while we updateing data..."
    static let loadingNew = "loading data..."

    static let connectionError = "connection Error"
    static let newDataComes = "New Data ,list will be updated"
    static let deliveriesList = "Deliveries List"
    static let deliverydet = "Delivery Details"


    
  
}


