//
//  AppDelegate.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 9/28/18.
//  Copyright © 2018 Ali Adam. All rights reserved.
//

import UIKit
import AlamofireNetworkActivityIndicator
import SVProgressHUD
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        createFirstController()
        setup()
        return true
    }
    //MARK: - Create First Controller 
    fileprivate func createFirstController() {
        // Create a SplashViewController and make it as an initial view controller.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let rootController = SplashBuilder.viewController()
        self.window?.rootViewController = rootController
        self.window?.makeKeyAndVisible()
    }
    //MARK: - setup them
    open func setup() {
        // Appearance Color Theme
        UINavigationBar.appearance().barTintColor = ColorsManager.brandColor
        UINavigationBar.appearance().tintColor = ColorsManager.whiteColor
        
        //Network Activity Indicator Manager
        setupNetworkIndicator()
        
        // SVProgressHUD Color Theme
        SVProgressHUD.setForegroundColor(ColorsManager.brandColor)
        SVProgressHUD.setBackgroundColor(ColorsManager.whiteColor)

    }
    private func setupNetworkIndicator() {
        NetworkActivityIndicatorManager.shared.isEnabled = true
        NetworkActivityIndicatorManager.shared.startDelay = 0.1
        NetworkActivityIndicatorManager.shared.completionDelay = 0.2
    }
}

