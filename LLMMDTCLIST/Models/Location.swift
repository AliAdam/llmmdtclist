
import Foundation
import RealmSwift
import Realm

@objcMembers class Location : BaseModel {

        dynamic var address : String?
        dynamic var lat = 0.0
        dynamic var lng  = 0.0

        enum CodingKeys: String, CodingKey {
                case address = "address"
                case lat = "lat"
                case lng = "lng"
        }
    
        public required convenience init(from decoder: Decoder) throws {
            self.init()
                let values = try decoder.container(keyedBy: CodingKeys.self)
                address = try values.decodeIfPresent(String.self, forKey: .address)
            lat = try values.decodeIfPresent(Double.self, forKey: .lat)!
            lng = try values.decodeIfPresent(Double.self, forKey: .lng)!
        }

}
