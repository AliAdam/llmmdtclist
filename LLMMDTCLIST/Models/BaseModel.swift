

import Foundation
import RealmSwift
import Realm
class BaseModel: RealmSwift.Object ,Codable{
    
    // MARK: Public Methods
    
    func update(with dictionary: [String: Any?]) {
        RealmService.shared.update(self, with: dictionary)
    }
      
    func create() {
        RealmService.shared.create(self)
    }
    
    func delete() {
        RealmService.shared.delete(self)
    }
}
