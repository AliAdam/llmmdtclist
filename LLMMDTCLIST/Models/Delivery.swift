

import Foundation
import RealmSwift
import Realm

typealias DeliveriesList = [Delivery]
@objcMembers class Delivery : BaseModel {

        dynamic var descriptionField : String?
        dynamic var id : Int = 0
        dynamic var isSelected  = false
        dynamic var imageUrl : String?
        dynamic var location : Location?
    
    override class func primaryKey() -> String? {
        return "id"
    }

        enum CodingKeys: String, CodingKey {
                case descriptionField = "description"
                case id = "id"
                case imageUrl = "imageUrl"
                case location = "location"
        }
    
        public required convenience init(from decoder: Decoder) throws {
            self.init()
                let values = try decoder.container(keyedBy: CodingKeys.self)
                descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
            id = try values.decodeIfPresent(Int.self, forKey: .id)!
                imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
            location = try values.decode(Location.self, forKey: .location)

        }

}
