//
//  NetworkProvider.swift
//  LLMMDTCLIST
//
//  Created by Ali Adam on 1/26/18.
//  Copyright © 2018 Ali Adam. All rights reserved.
//

import UIKit
import  Alamofire

/// shard singlton act like a network layer get and update the data
struct NetworkProvider {
    static let shared = NetworkProvider()
    static let timeout: TimeInterval = 30.0
    let alamofireManager: SessionManager
    
    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = NetworkProvider.timeout
        configuration.timeoutIntervalForRequest = NetworkProvider.timeout
        alamofireManager = SessionManager(configuration: configuration)
        
        
    }

    // load delivery list
    @discardableResult func getDeliveresList(offset:Int ,limit:Int ,completionHandler: @escaping (NetworkResponse<DeliveriesList>) -> ()) -> DataRequest {
        let parameters = JsonBodyHelper.formatBodyForDeliveresList(offset: offset,limit: limit)
        let request = alamofireManager.request(NetworkRouter.deliveries(parameters))
        request.responseDeliveresList { (response ) in
            if let deliveresList = response.result.value {
                    completionHandler(.success(deliveresList))
            }
            else
            {
                let error =  self.parseNetworkError(responseError: response.error!)
                completionHandler(.error(error))
            }
        }
        return request
    }
    
    // parse error and print class name and function name and line where the error occures
    private func parseNetworkError(responseError:Error ,functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) -> Error {
        let className = (fileName as NSString).lastPathComponent
        debugPrint("error in \(className)  \(functionName) \(lineNumber)")
        if let err = responseError as? URLError, err.code == .notConnectedToInternet {
            let error =  createModelError(errorMessage: LocalizableWords.connectionError)
            return error
        }
        return responseError
    }
    
    private func createModelError(errorMessage:String?) -> Error {
        if let msg = errorMessage {
        let error = NSError(domain:"", code:400, userInfo:[ NSLocalizedDescriptionKey:msg])
        return error
        }
        let error = NSError(domain:"", code:400, userInfo:[ NSLocalizedDescriptionKey:LocalizableWords.connectionError])
        return error
    }
}


// MARK: - Alamofire response handlers
extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            return Result { try JSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
@discardableResult
    func responseDeliveresList(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<DeliveriesList>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
}
