import Alamofire

enum NetworkRouter: URLRequestConvertible {
    
    private enum Constants {
        static let basePath = ""
        static let version = ""
    }
   
    case deliveries(Parameters)
   

    var method: HTTPMethod {
        switch self{
        case .deliveries:
            return .get
//        default :
//            return .post
            
            
        }
    }
    var path: String {
        switch self {
            
        case .deliveries:
            return "deliveries"
        }
    }
    func getHTTPHeaders() -> HTTPHeaders? {
        return ["Content-Type" : "application/json"]
    }
    func asURLRequest() throws -> URLRequest {
        
        let url:URL  = URL.baseServerURL!.appendingPathComponent(Constants.basePath + Constants.version)
        var urlRequest: URLRequest = URLRequest(url: url.appendingPathComponent(path))
             urlRequest.httpMethod = method.rawValue
             urlRequest.allHTTPHeaderFields = getHTTPHeaders()
        
        switch self {
        case .deliveries(let params):
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: params)
        }


    }
}
