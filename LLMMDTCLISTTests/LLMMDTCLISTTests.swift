//
//  LLMMDTCLISTTests.swift
//  LLMMDTCLISTTests
//
//  Created by Ali Adam on 9/28/18.
//  Copyright © 2018 Ali Adam. All rights reserved.
//

import XCTest
@testable import LLMMDTCLIST

class LLMMDTCLISTTests: XCTestCase {

    override func setUp() {
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewModels() {
        var listViewModel = ListViewModel()
        // test empty list
        XCTAssertEqual(listViewModel.deliveriesList.value.count, 0)
       let mockUpList =  getMocupList()
        // test with mockup list
        listViewModel = ListViewModel(deliveriesList:mockUpList)
        XCTAssertEqual(listViewModel.deliveriesList.value.count, mockUpList.count)
        // test selection on list and details viewmodels
        listViewModel.updateSelectedItem(with: 0)
        let detailsViewModel = DetailsViewModel()
        detailsViewModel.loadSelectedItem()
        XCTAssertEqual(detailsViewModel.deliveryItem.value.id, mockUpList.first?.id)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    
    
    func getMocupList()->[Delivery]{
        let par = [["id":0,"description":"Deliver documents to Andrio","imageUrl":"https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-7.jpeg","location":["lat":22.319181,"lng":114.170008,"address":"Mong Kok"]],["id":1,"description":"Deliver documents to Andrio","imageUrl":"https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg","location":["lat":22.336093,"lng":114.155288,"address":"Cheung Sha Wan"]]]
        let jsonData = try? JSONSerialization.data(withJSONObject: par, options: [])
        let decoder = JSONDecoder()
        let list = try! decoder.decode([Delivery].self, from: jsonData!)
        print(list)
        return list
    }
    

}
