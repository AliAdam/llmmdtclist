### Deliveries List  APP ,  MVVM  implementation in iOS 

### MVVM architecture + Router
MVVM (Model-View-ViewModel) is derived from MVC(Model-View-Controller).
It is introduced to solve existing problems of Cocoa's MVC architecture in iOS world.
One of its feature is to make a better seperation of concerns so that it is easier to maintain and extend.

*  Model: It is simillar to `model` layer in MVC (contains data business logic)

* View: UIViews + UIViewControllers 

* ViewModel: A mediator to glue two above layer together.

* Router: When the user taps  to navigate to the next scene . A router extracts this navigation logic out of the view controller. It is also the best place to pass any data to the next scene. As a result, the view controller is left with just the task of controlling views.

* Builder: initialize ViewController, ViewModel and Router
provide the ViewController to the outside world (via the viewController() method)
 if the Scene needs some initial data it is injected into the Builder who then injects it into the ViewModel


An important point in MVVM is that it uses a binder as communication tool between View and ViewModel layers.
A technique named `Data Binding` is used. 

### Maping data to model using  ```Codable```

### offline data will be loaded first user can use it  till new data come .

#### Libraries
* [Alamofire] to perform HTTP requests under `Model` layer.
* [RxSwift + RxCocoa] to do "data binding" job which binds `ViewModel` and `View`
* [Realm] to persit local data
* [SnapKit]  to make Auto Layout easy on  iOS
* [SDWebImage] to download images and cach it 
* [ESPullToRefresh]  to add pull to refresh and load more to tableView
* [ReachabilitySwift] to listen to network status change 
* [SVProgressHUD] to show toast and loading indicator  
